# This is a Apache Nifi test template

My first template exported from Apache Nifi.

## TL;DL

This Apache Nifi flow template redirects an email, where the reciever is specified in the initial emails subject. Furthermore it can upload the generate Apache Nifi flow file to an specified FTP and write it to the Apache Nifi Docker volume.

So i.e. sending an email to `consumer@mydomain.tld` with the subject `reciever@mydomain.tld`, Apache Nifi will send an email `reciever@mydomain.tld`.

## Prerequisits

1. Install and run Docker container of Apache Nifi (tested with version 1.8.0): https://hub.docker.com/r/apache/nifi
2. Provide an email address where you can recieve emails via POP3 and send emails via SMTP. Because of advanced security features, an Gmail address is not recommended
3. Optional disk space accessible via SFTP

## Steps in flow

This flow contains following steps:

1. Consume POP3 (consumed email will be deleted)
2. Extract email headers
3. Send email to address specified in subject
4. Rename the flow file
5. Put the flow file to an specified disk space via SFTP
6. Write the flow file to Apache Nifi volume

Ignore the LogMessage processor!

## Configuration

Load the flow template into Apache Nifi and configure the processors.

1. Configure the POP3 processor with the reciever email inbox (host, port, credentials)
2. Configure the PutMail processor with the sender email address (host, port, credentials, "From"). Alter the subject or message as you please. It is necessary that the value `${email.headers.subject}` is provided in the "To" field.
3. Configure the PutSFTP processor with you SFTP credentials. If the property "Remote Path" is left empty, the flow file will be transfered to the FTP users home directory. Make sure to set "Create Directory" to `true`, if you are not setting up the directory in the FTP path.
4. Optionally configure the PutFile processor. ATM it is configured to save the file to the path '/opt/nifi/nifi-current/test/`. This directory will be created if non-existent. The users and usergroups name of the folder and file will be `nifi` and it's permission is set to 777. Change it as you please.
